const Koa           = require('koa');
const app           = new Koa();
const router        = require('@koa/router')();
const bodyParser    = require('koa-bodyparser');
const omdb          = require('./omdb/omdb')

app.use(bodyParser());

// método get que va a buscar peliculas
router.get('/buscarPelicula', async (ctx, next) => {
    console.log('Ingresa a buscarPelicula');

    //variable que rescata lo que el usuario ingresó por url
    const buscador = ctx.request.query.buscador;

    //variable que rescata el año de la película
    let anoPelicula = ctx.request.header.anopelicula;

    console.log(`pelicula a buscar: ${ buscador }`)

    //valida que la variable esté declarada en la url
    if(buscador){
        
        // asigna valor valido en caso de que no venga definido el año en header
        if(!anoPelicula){
            anoPelicula = 0;
            console.log(`No se detectó año en el header`)
        } else {
            console.log(`Año a consultar ${ anoPelicula }`)
        }

        //comienza la lógica de negocio
        const obj = await omdb.buscarPelicula(buscador, anoPelicula);

        ctx.body    = obj;
        ctx.status  = 200;
        
    }else{
        console.log('buscador no definido');
        ctx.response.status  = 400;
        ctx.response.message  = "Debe indicar en la variable 'buscador' la película que desa buscar.";
    }

    
    next();
});

//retorna todas las peliculas insertadas en la bd
router.get('/buscarTodas', async (ctx, next) => {
    console.log('Ingresa a buscar todas las peliculas')

    // rescata el valor del paginado que viene por header
    let numPagina = ctx.request.header.numpagina;

    let datos;

    if( !numPagina ){
        numPagina = 0;
    } 

    console.log(`Número de página: ${ numPagina }`)
    
    //va a buscar los datos a bd
    datos = await omdb.buscarTodas(numPagina) 
    ctx.body = datos;

    next();
});

//va a buscar a la bd un objeto y modifica el plot
router.post('/modificarPelicula', async (ctx) => {

    const obj = ctx.request.body;

    if( obj ){
        ctx.body            = obj;
        const modificar     = await omdb.modificar(obj);

        ctx.body    = { "nuevoPlot" :  modificar };
        ctx.status  = 200;
    } else {
        console.log('Objeto en el body no válido');
        ctx.response.status  = 400;
        ctx.response.message  = "Debe indicar en la variable 'buscador' la película que desa buscar.";
        ctx.response.body   = { 'error' : 'Objeto en el body no válido', 'status': 400 };
    }
})


router.get('/', (ctx) => {
    ctx.body = "Bienvenido a MovieNode Challenge!";
});

app.use(router.routes());
app.listen(3000);

console.log('Aplicación lista para usar');
