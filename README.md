# Movie Challenge - CLM Consultores

En este desafío demuestro mis habilidades con framework JavaScript Node.js utilizando MongoDB como base de datos. Se utilizó como servidor KOA y como ORM Mongoose. Esta aplicación cuenta con tres endpoints los cuales se detallan a continuación.

# Endpoints

- /buscarPelícula : va a buscar una película a OMDB.com, inserta en la base de datos y muestra los resultados
- /buscarTodas: retorna todas las películas registradas en la base de datos, con un máximo de 5 registros.
- /modificarPelicula (post): modifica el plot de una película

## Instalacion

MovieChallenge necesita tener instalado [Node.js](https://nodejs.org/) v10+ y [MongoDB](https://docs.mongodb.com/manual/installation/).

Para ejecutar MovieChallenge, debes seguir las siguientes instrucciones:

```sh
git clone https://gitlab.com/kevinguzman.dev/movie-challenge.git
cd movie-challenge
npm install
npm start
```

## Uso
Para ejecutar la aplicación debes ingresar por Postman al siguiente enlace:

```sh
    http://localhost:3000
```

### ENDPOINTS
| ENDPOINT | TIPO | PARÁMETROS | DESCRIPCIÓN | EJEMPLO |
|----------| ---- | ---------  | ----------- | ------- |
| buscarPelicula | GET | buscador  | Busca una película y la inserta en la base de datos. | http://localhost:3000/buscarPelicula?buscador=cars |
| buscarTodas | GET | numpagina (header)  | Retorna todas las películas registradas en la base de datos, puedes paginar enviando el número de página por header. | http://localhost:3000/buscarTodas |
| modificarPelicula | POST | detallado más abajo  | Modifica el campo plot de un registro existente en la base de datos. Debes enviar un JSON en el body de la request. | http://localhost:3000/modificarPelicula |

Body de modificarPelícula

```sh
{
    "movie": "Yellow", 
    "find": "Family", 
    "replace": "STRING" 
}
```








