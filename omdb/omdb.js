const request       = require('request');
const conexion      = require('../models/conexion')
const movie         = require('../models/movie');

//va a buscar la película a omdb
const buscarPelicula = (buscador, anoPelicula = 0) => {
    //api key obtenida desde omdb
    const apikey        = 'f55dd269';

    //url donde se enviará la petición GET
    const url = `http://www.omdbapi.com/?apikey=${ apikey }&t=${ buscador }`;

    //concatena el año en caso de que sea valido
    if(anoPelicula != 0){
        console.log(`Año pelicula: ${ anoPelicula }`)
        url += `&y=${ anoPelicula }`;
    }

    //ejecuta petición GET
    request(url, { json : true} , ( err, res, body ) => {
        if(err) { return console.log(err); }

        //conecta a mongo
        conexion.conexion;

        //genera instancia del modelo que apunta a mongo
        let movieOmdb = new movie({
            title: body.Title,
            year: body.Year,
            released: body.Released,
            genre: body.Genre,
            director: body.Director,
            actors: body.Actors,
            plot: body.Plot,
            ratings: body.Ratings
        });

        //obtiene el titulo de la película para filtrar en la bd
        const tituloPelicula = body.Title;

        return new Promise( (resolve, reject) => {
            console.log(tituloPelicula);
            
            //consulta si existe objeto en la bd
            let consulta = movie.find( { 'title' :  tituloPelicula } );
            
            //ejecuta la query a la bd
            consulta.exec( (error, resp) => {
                if(error) reject(error);

                //si la consulta no tiene length, entonces inserta
                if(!resp.length){
                    movieOmdb.save( (err, res) => {
                        if(err) reject(err);
        
                        console.log('Pelicula insertada con éxito en la BD');
                        //console.log(res);
        
                        resolve(res);
                    });  
                }else{
                    console.log('Película ya fué almacenada en la BD');
                    resolve(resp);
                }
            });
        });
    });
}

const buscarTodas = ( numPagina ) => {
    //conecta a mongo
    conexion.conexion;

    return new Promise((resolve, reject) => {
        movie.find({} , (error, data) => {
            if(error){ reject(error) }
            
            resolve(data);
        }).limit(5);
    });
}


const modificar = ( objeto ) => {
    // conecta a mongo
    conexion.conexion;

    // constante que nos facilitara obtener la busqueda
    const find      = objeto.find;
    const titulo    = objeto.movie;
    const replace   = objeto.replace;
    
    // promesa que controlará el bloque
    let promesa     = new Promise((resolve, reject) => {
        
        // consulta si existe objeto en la bd
        let consulta = movie.find({ "title" : titulo });
        
        //ejecuta query
        consulta.exec( (error, respuesta) => {
            if(error) { reject(error) }
            let nuevoPlot = '';

            // recorre para buscar cada uno de los registros que coincidan con la busqueda
            respuesta.forEach(objRespuesta => {
                let plot        = objRespuesta.plot;
                nuevoPlot       = plot.replace(find, replace);

                objRespuesta.plot = nuevoPlot;
                
                //busca por id y lo modifica
                movie.findByIdAndUpdate(objRespuesta._id, objRespuesta, { new:  true, runValidators:  true }).exec( (err, resp) => {
                    if(err) reject(err)
                    
                    resolve(resp.plot);
                });
            });
        });
    }) ;

    return promesa;
}

module.exports = {
    buscarPelicula,
    buscarTodas,
    modificar
}
