var mongo   = require('mongoose');

//genera conexión con mongo
const conexion = mongo.connect('mongodb://localhost:27017/movieChallenge', {useNewUrlParser: true, useUnifiedTopology: true})
        .then(() => console.log("Conexión a mongo exitosa") )
        .catch(err=> console.log(err));

module.exports = { conexion }