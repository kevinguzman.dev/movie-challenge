var mongo   = require('mongoose');
var schema  = mongo.Schema; 

const movie = new schema({
    title : String,
    year: Number,
    released: String,
    genre: String,
    director: String,
    actors: String,
    plot: String,
    ratings: [{ Source: String, Value: String}]
});

module.exports = mongo.model('movie', movie);